import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

app.get('/task2c', (req, res) => {
	console.log(req.query.username);
	req.query.username.toString();
	var str = req.query.username.match(/(?:(?:http[s]?:\/{2})|\/{2}|(?:.+)\/)?(?:@)?(?:.+?\/)?(?:@?)([\w._]+)/i) || [];
	res.send('@'+str[1]);
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
